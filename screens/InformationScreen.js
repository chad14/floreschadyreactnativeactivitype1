import React, {Component} from 'react';
import { StyleSheet, Text, View, Button } from 'react-native';

class InformationScreen extends Component {
    render() {
        return (
          <View style={styles.container}>
           
            <Text>Name: Flores, Chady R.</Text>
            <Text>Age: 19 </Text>
            <Text>Email: chadyflores14@gmail.com </Text>
            <Text>Course: Bachelor of Science in Information Technology</Text>
            <Text>Section: BSIT-3B</Text>
            <Text>ICLC: Treasurer</Text>
            <Text>Cute</Text>
            <Button
              title="Home"
              onPress={() => this.props.navigation.navigate('Home')}
            />
          </View>
        );
    }
}

const styles = StyleSheet.create({
    container: {
      flex: 1,
      backgroundColor: '#fff',
      alignItems: 'center',
      justifyContent: 'center',
    },
  });

const fontSize = StyleSheet.create({
    container: {
      fontSize: 20, 
    },
  });


export default InformationScreen;